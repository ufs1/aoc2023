pub fn run(day_num: i32, input: String) {
    match day_num {
        1 => day1::run(input),
        2 => day2::run(input),
        3 => day3::run(input),
        4 => day4::run(input),
        5 => day5::run(input),
        6 => day6::run(input),
        7 => day7::run(input),
        _ => println!("Day {} not implemented", day_num),
    }
}

// when we want logic to branch for different parts in one day
#[derive(PartialEq, Eq)]
enum Part {
    PartOne,
    PartTwo,
}

mod day1 {

    pub fn run(input: String) {
        use regex::Regex;
        let re_d = Regex::new(r"\d").unwrap();
        let re = Regex::new(r"\d|one|two|three|four|five|six|seven|eight|nine").unwrap();
        let re_rev = Regex::new(r"\d|eno|owt|eerht|ruof|evif|xis|neves|thgie|enin").unwrap();

        let ans = input.lines().fold((0, 0), |x, l| {
            (
                x.0 + (parse_digit(re_d.find(l).unwrap().as_str()) * 10
                    + parse_digit(
                        re_d.find(&l.chars().rev().collect::<String>())
                            .unwrap()
                            .as_str(),
                    )),
                x.1 + (parse_digit(re.find(l).unwrap().as_str()) * 10
                    + parse_digit(
                        re_rev
                            .find(&l.chars().rev().collect::<String>())
                            .unwrap()
                            .as_str(),
                    )),
            )
        });
        println!("{:?}", ans);
    }
    fn parse_digit(s: &str) -> i32 {
        match s {
            "one" | "eno" => 1,
            "two" | "owt" => 2,
            "three" | "eerht" => 3,
            "four" | "ruof" => 4,
            "five" | "evif" => 5,
            "six" | "xis" => 6,
            "seven" | "neves" => 7,
            "eight" | "thgie" => 8,
            "nine" | "enin" => 9,
            _ => s.parse::<i32>().unwrap_or_else(|_| panic!("{:?}", s)),
        }
    }
}

mod day2 {

    pub fn run(input: String) {
        let games = input.lines().enumerate();
        let mut ans0 = 0;
        let mut ans1 = 0;
        let total_collection: Draw = Draw {
            green: 13,
            blue: 14,
            red: 12,
        };

        for g in games {
            let start = g.1.find(':').unwrap() + 2;
            // part 1
            if g.1[start..]
                .split(';')
                .all(|subgame_str| is_possible(&mk_collection(subgame_str), &total_collection))
            {
                ans0 += g.0 + 1;
            };
            // part 2
            use std::cmp::max;
            let maxs = g.1[start..].split(';').fold(
                Draw {
                    green: 0,
                    blue: 0,
                    red: 0,
                },
                |acc, subgame| {
                    let d = mk_collection(subgame);
                    Draw {
                        green: max(acc.green, d.green),
                        blue: max(acc.blue, d.blue),
                        red: max(acc.red, d.red),
                    }
                },
            );
            ans1 += maxs.green * maxs.blue * maxs.red;
        }
        println!("{:?} {:?}", ans0, ans1);
    }

    #[derive(Debug)]
    struct Draw {
        green: i32,
        blue: i32,
        red: i32,
    }

    use lazy_static::lazy_static;
    use regex::Regex;
    lazy_static! {
        static ref GREEN_RE: Regex = Regex::new(r"(\d+) green").unwrap();
        static ref BLUE_RE: Regex = Regex::new(r"(\d+) blue").unwrap();
        static ref RED_RE: Regex = Regex::new(r"(\d+) red").unwrap();
    }

    fn mk_collection(s: &str) -> Draw {
        let green = GREEN_RE
            .captures(s)
            .map(|m| m[1].parse::<i32>().unwrap_or(0))
            .unwrap_or(0);
        let blue = BLUE_RE
            .captures(s)
            .map(|m| m[1].parse::<i32>().unwrap_or(0))
            .unwrap_or(0);
        let red = RED_RE
            .captures(s)
            .map(|m| m[1].parse::<i32>().unwrap_or(0))
            .unwrap_or(0);

        Draw { green, blue, red }
    }

    fn is_possible(d: &Draw, tot: &Draw) -> bool {
        d.green <= tot.green && d.blue <= tot.blue && d.red <= tot.red
    }
}

mod day3 {
    use std::collections::HashMap;

    pub fn run(input: String) {
        use regex::Regex;
        // make a usable grid of chars
        let schematic: Vec<Vec<char>> = input.lines().map(|l| l.chars().collect()).collect();
        let num_re: Regex = Regex::new(r"\d+").unwrap();
        let s = input.lines().enumerate().collect::<Vec<(usize, &str)>>();
        let mut ans0 = 0;
        let mut ans1 = 0;
        let mut gears: HashMap<(i32, i32), (i32, i32)> = HashMap::new();
        // find the number entries; determine whether they are parts (keep a
        // running sum of values if so) and whether they are connected to
        // another entry by a gear (keep a running sum if so)
        for l in s {
            for e in num_re.find_iter(l.1) {
                let val = e.as_str().parse::<i32>().unwrap();
                let pos_y = l.0;
                let pos_x = e.start();
                let pos_x_end = e.end();
                let mut is_part_num = false;
                for y in pos_y as i32 - 1..=pos_y as i32 + 1 {
                    for x in pos_x as i32 - 1..=pos_x_end as i32 {
                        if x >= 0
                            && x < l.1.len() as i32
                            && y >= 0
                            && y < schematic.len() as i32
                            && is_symbol(&schematic[y as usize][x as usize])
                        {
                            is_part_num = true; // part 1
                            if schematic[y as usize][x as usize] == '*' {
                                // part 2
                                ans1 += add_to_gear(y, x, val, &mut gears);
                            }
                        }
                    }
                }
                if is_part_num {
                    ans0 += val;
                }
            }
        }
        println!("{:?}", ans0);
        println!("{:?}", ans1);
    }

    fn is_symbol(c: &char) -> bool {
        c.is_ascii_punctuation() && c != &'.'
    }

    fn add_to_gear(y: i32, x: i32, v: i32, g: &mut HashMap<(i32, i32), (i32, i32)>) -> i32 {
        let new_entry = match g.get(&(y, x)) {
            Some((count, ratio)) => (count + 1, ratio * v),
            None => (1, v),
        };
        g.insert((y, x), new_entry);
        if new_entry.0 == 2 {
            new_entry.1
        } else if new_entry.0 == 3 {
            // a gear connects exactly two parts, so undo an erroneous earlier
            // addition if there's a third part next to the '*'
            -new_entry.1
        } else {
            0
        }
    }
}

mod day4 {

    pub fn run(s: String) {
        let scores: Vec<u32> = get_scores(&s);
        // part 1
        let ans0 = scores
            .iter()
            .map(|sc| {
                if *sc == 0 {
                    0
                } else {
                    2_i32.checked_pow(sc - 1).unwrap()
                }
            })
            .sum::<i32>();
        // part 2
        let mut cards = vec![1; scores.len()];
        for i in 0..cards.len() {
            for j in 1..=scores[i] {
                let j = j as usize;
                if i + j < cards.len() {
                    cards[i + j] += cards[i];
                }
            }
        }
        let ans1: i32 = cards.iter().sum();
        println!("{:?} {:?}", ans0, ans1);
    }

    // the "score" does different things in parts 1 and 2, but in both cases
    // it's the overlap in numbers between the numbers on the first part of the
    // card and the second
    fn get_scores(s: &str) -> Vec<u32> {
        use regex::Regex;
        use std::collections::HashSet;
        let re_d = Regex::new(r"\d+").unwrap();
        s.lines()
            .map(|l| {
                let l_ = &l[l.find(':').unwrap() + 1..];
                let chunks: Vec<&str> = l_.split('|').collect();
                let mut winning: HashSet<i32> = HashSet::from_iter(
                    re_d.find_iter(chunks[0])
                        .map(|n| n.as_str().parse::<i32>().unwrap()),
                );
                let have: HashSet<i32> = HashSet::from_iter(
                    re_d.find_iter(chunks[1])
                        .map(|n| n.as_str().parse::<i32>().unwrap()),
                );
                winning.retain(|n| have.contains(n));
                winning.len() as u32
            })
            .collect::<Vec<u32>>()
    }
}

mod day5 {
    use crate::util::{cmp_intervals, extract_nums_u64};
    use std::iter::Peekable;

    pub fn run(input: String) {
        let mut s = input.lines().peekable();
        let seeds = extract_nums_u64(s.next().unwrap());
        let mut next_type0 = vec![]; // pt 1 initial values
        let mut next_type1 = vec![]; // pt 2 initial values
        let mut i = 0;
        loop {
            if i >= seeds.len() {
                break;
            }
            next_type0.push((seeds[i], seeds[i] + 1));
            if i % 2 == 0 {
                next_type1.push((seeds[i], seeds[i] + seeds[i + 1]));
            }
            i += 1;
        }

        loop {
            if s.peek().is_none() {
                break;
            }
            let rules = get_rules(&mut s);
            next_type0 = apply_rules(&rules, &next_type0);
            next_type1 = apply_rules(&rules, &next_type1);
        }
        println!("{:?}", next_type0.iter().min().unwrap().0);
        println!("{:?}", next_type1.iter().min().unwrap().0);
    }

    fn get_rules(i: &mut Peekable<std::str::Lines<'_>>) -> Vec<(u64, u64, u64)> {
        let mut rules = vec![];
        loop {
            // consume lines until actual data starts
            let s = i.next().unwrap();
            if s.contains("map") {
                break;
            }
        }
        loop {
            let s = i.next();
            if s.is_none() || s.unwrap().is_empty() {
                break;
            }
            let t = extract_nums_u64(s.unwrap());
            rules.push((t[1], t[2], t[0]));
        }
        rules
    }

    fn apply_rules(rules: &Vec<(u64, u64, u64)>, vals: &[(u64, u64)]) -> Vec<(u64, u64)> {
        let mut old_vals = vec![(0, 0); vals.len()];
        let mut out_vals = vec![];
        old_vals.clone_from_slice(vals);
        for rule in rules {
            let rule_int = (rule.0, rule.0 + rule.1);
            let mut remainder = vec![];
            for val in &mut old_vals {
                let [intersection, diff0, diff1] = cmp_intervals(*val, rule_int);
                if intersection == (0, 0) {
                    continue;
                }
                let delta: i64 = <u64 as TryInto<i64>>::try_into(rule.2).unwrap()
                    - <u64 as TryInto<i64>>::try_into(rule.0).unwrap();
                out_vals.push((
                    intersection.0.checked_add_signed(delta).unwrap(),
                    intersection.1.checked_add_signed(delta).unwrap(),
                ));
                *val = diff0;
                if diff1 != (0, 0) {
                    remainder.push(diff1);
                }
            }
            old_vals.append(&mut remainder);
            old_vals.retain(|t| *t != (0, 0));
        }
        out_vals.append(&mut old_vals);
        out_vals
    }
}

mod day6 {
    use crate::util::extract_nums_u64;

    pub fn run(s: String) {
        let mut s = s.lines();
        let t_str = s.next().unwrap();
        let times = extract_nums_u64(t_str); // pt 1
        let time = extract_nums_u64(&t_str.replace(' ', "")); // pt 2
        let d_str = s.next().unwrap();
        let distances = extract_nums_u64(d_str);
        let distance = extract_nums_u64(&d_str.replace(' ', ""));
        let mut ans0 = 1.0;
        for i in 0..times.len() {
            ans0 *= soltns(times[i], distances[i]);
        }
        let ans1 = soltns(time[0], distance[0]);
        println!("{:?} {:?}", ans0, ans1);
    }
    fn soltns(t: u64, d: u64) -> f64 {
        let disc = (t * t - 4 * d) as f64;
        let disc_sq_root = f64::sqrt(disc);
        let t = t as f64;
        let r0 = (-t + disc_sq_root) / -2.0;
        let r1 = (-t - disc_sq_root) / -2.0;
        // point is to get the next whole number after or before the root
        // and then get the number of numbers in the range, inclusively
        ((r1 - 1.0).ceil() - (r0 + 1.0).floor()).abs() + 1.0
    }
}

mod day7 {

    use crate::Part;

    pub fn run(s: String) {
        println!(
            "{:?} {:?}",
            solve(&s, Part::PartOne),
            solve(&s, Part::PartTwo)
        );
    }

    fn solve(s: &str, p: Part) -> i32 {
        let mut inp = parse_input(s.to_string(), &p);
        inp.sort_by(|a, b| {
            if a.0.rank.cmp(&b.0.rank) != std::cmp::Ordering::Equal {
                a.0.rank.cmp(&b.0.rank)
            } else {
                card_cmp(&a.0.hand, &b.0.hand, &p)
            }
        });
        inp.iter()
            .enumerate()
            .fold(0, |acc, (place, (_hand, bid))| {
                acc + (place + 1) as i32 * bid
            })
    }

    #[derive(Debug)]
    struct Hand {
        hand: String,
        rank: Rank,
    }

    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
    enum Rank {
        High,
        OnePair,
        TwoPair,
        ThreeOfAKind,
        FullHouse,
        FourOfAKind,
        FiveOfAKind,
    }

    fn partition<T: Eq + Ord + Copy>(mut inp: Vec<T>) -> Vec<Vec<T>> {
        inp.sort();
        let mut toret = vec![];
        let mut chunk = vec![];
        for e in inp {
            if chunk.is_empty() || e == chunk[0] {
                chunk.push(e);
            } else {
                toret.push(chunk.clone());
                chunk.clear();
                chunk.push(e);
            }
        }
        toret.push(chunk.clone()); // last chunk
        toret.sort_by_key(|a| a.len());
        toret.reverse();
        toret
    }

    fn mk_hand(s: &str, p: &Part) -> Hand {
        assert!(s.len() == 5);
        let mut s_c: Vec<char> = s.chars().collect();
        let mut cards;

        if *p == Part::PartTwo {
            let joker_count = s_c.iter().filter(|c| **c == 'J').count();
            s_c.retain(|c| *c != 'J');
            cards = partition(s_c);
            for _ in 0..joker_count {
                cards[0].push('J');
            }
        } else {
            cards = partition(s_c);
        }

        let rank = match cards.len() {
            5 => Rank::High,
            4 => Rank::OnePair,
            3 => {
                if cards[0].len() == 3 {
                    Rank::ThreeOfAKind
                } else {
                    Rank::TwoPair
                }
            }
            2 => {
                if cards[0].len() == 3 {
                    Rank::FullHouse
                } else {
                    Rank::FourOfAKind
                }
            }
            1 => Rank::FiveOfAKind,
            _ => panic!(),
        };
        Hand {
            hand: s.to_string(),
            rank,
        }
    }

    #[derive(PartialEq, Eq, PartialOrd, Ord)]
    enum Card {
        J1,
        N2,
        N3,
        N4,
        N5,
        N6,
        N7,
        N8,
        N9,
        T,
        J2,
        Q,
        K,
        A,
    }

    fn mk_card(c: &char, p: &Part) -> Card {
        match c {
            'J' => {
                if *p == Part::PartOne {
                    Card::J2
                } else {
                    Card::J1
                }
            }
            '2' => Card::N2,
            '3' => Card::N3,
            '4' => Card::N4,
            '5' => Card::N5,
            '6' => Card::N6,
            '7' => Card::N7,
            '8' => Card::N8,
            '9' => Card::N9,
            'T' => Card::T,
            'Q' => Card::Q,
            'K' => Card::K,
            'A' => Card::A,
            _ => panic!(),
        }
    }

    fn card_cmp(a: &str, b: &str, p: &Part) -> std::cmp::Ordering {
        let a_c = a.chars();
        let b_c_v: Vec<char> = b.chars().collect();
        let mut toret = std::cmp::Ordering::Equal;
        for (i, l) in a_c.enumerate() {
            let let_cmp = mk_card(&l, p).cmp(&mk_card(&b_c_v[i], p));
            if let_cmp != std::cmp::Ordering::Equal {
                toret = let_cmp;
                break;
            } else {
                continue;
            }
        }
        if toret == std::cmp::Ordering::Equal {
            panic!();
        }
        toret
    }

    fn parse_input(s: String, p: &Part) -> Vec<(Hand, i32)> {
        let mut toret = vec![];
        for l in s.lines() {
            let mut parts = l.split(' ');
            let hand = mk_hand(parts.next().unwrap(), p);
            let bid: i32 = parts.next().unwrap().parse().unwrap();
            toret.push((hand, bid));
        }
        toret
    }
}

mod util {

    pub fn extract_nums_u64(s: &str) -> Vec<u64> {
        use lazy_static::lazy_static;
        use regex::Regex;
        lazy_static! {
            static ref INT_RE: Regex = Regex::new(r"\d+").unwrap();
        }
        INT_RE
            .find_iter(s)
            .map(|m| m.as_str().parse::<u64>().unwrap())
            .collect::<Vec<_>>()
    }

    pub fn cmp_intervals(a: (u64, u64), b: (u64, u64)) -> [(u64, u64); 3] {
        // returns [a intersect b, a \ b (in two intervals)]
        // let mut toret = ((0, 0), (0, 0), (0, 0));
        use std::cmp::{max, min};
        let intersection = (max(a.0, b.0), min(a.1, b.1));

        let mut toret = if intersection.1 <= intersection.0 {
            [(0, 0), (a.0, a.1), (0, 0)]
        } else if intersection.0 == a.0 {
            [intersection, (intersection.1, a.1), (0, 0)]
        } else if intersection.1 == a.1 {
            [intersection, (a.0, intersection.0), (0, 0)]
        } else {
            [intersection, (a.0, intersection.0), (intersection.1, a.1)]
        };
        for item in toret.iter_mut().skip(1) {
            if item.0 >= item.1 {
                *item = (0, 0);
            }
        }
        toret
    }
}
