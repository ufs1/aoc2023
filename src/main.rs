fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() <= 1 {
        println!("Need day number and (if applicable) filename of input as arguments");
        std::process::exit(1);
    }

    let day_num: i32 = args[1].parse().unwrap_or_else(|_| {
        println!("Invalid day number.");
        std::process::exit(1);
    });

    if !(1..=25).contains(&day_num) {
        println!("Day number out of range.");
        std::process::exit(1);
    }

    let mut input: String = "".to_string();

    if args.len() >= 3 {
        use std::fs;
        input = fs::read_to_string(&args[2]).unwrap_or_else(|_| {
            println!("Could not find input file");
            std::process::exit(1);
        });
    }

    use aoc2023::run;
    run(day_num, input);
}
